package java.ro.alexmus.softvision.spring.boot.taskletexample;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class TaskletExampleConfiguration {
    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private TaskletExample tasklet;

    @Bean(name="taskletJob")
    public Job job() {
        return jobs.get("taskletJob")
                .start(step())
                .build();
    }

    @Bean(name="taskletStep")
    public Step step() {
        return stepBuilderFactory.get("taskletStep")
                .tasklet(tasklet)
                .build();
    }

}

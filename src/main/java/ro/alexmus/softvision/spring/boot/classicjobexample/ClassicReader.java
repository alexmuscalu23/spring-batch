package java.ro.alexmus.softvision.spring.boot.classicjobexample;


import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Iterator;

@Component
public class ClassicReader implements ItemReader<String> {

    @Value("${classicJob.upperBound}")
    private Long upperBound;

    private Iterator<String> values = Arrays.asList("1","2","3","4","5","6","7").iterator();

    @Override
    public String read() throws Exception {
        if (!values.hasNext()){
            return null;
        }
        return values.next();
    }
}

package java.ro.alexmus.softvision.spring.boot.classicjobexample;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class ClassicProcessor implements ItemProcessor<String, Long>{
    @Override
    public Long process(String s) throws Exception {
        return Long.valueOf(s);
    }
}

package java.ro.alexmus.softvision.spring.boot.classicjobexample;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class ClassicWriter implements ItemWriter<Long>{
    @Override
    public void write(List<? extends Long> list) throws Exception {
        log.info("{}",list);
    }
}

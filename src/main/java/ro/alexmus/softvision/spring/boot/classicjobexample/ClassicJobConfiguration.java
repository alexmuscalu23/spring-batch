package java.ro.alexmus.softvision.spring.boot.classicjobexample;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ClassicJobConfiguration {
    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ClassicReader classicReader;

    @Autowired
    private ClassicProcessor classicProcessor;

    @Autowired
    private ClassicWriter classicWriter;

    @Bean(name="classicJob")
    public Job job() {
        return jobs.get("classicJob")
                .start(step())
                .build();
    }

    @Bean(name="classicJobStep")
    public Step step() {
        return stepBuilderFactory.get("classicJobStep")
                .<String, Long>chunk(3)
                .reader(classicReader)
                .processor(classicProcessor)
                .writer(classicWriter)
                .build();
    }
}
